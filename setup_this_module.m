%% Setup this (support) module for generic use cases.
%
% The "users" are typically setup code of other modules (repositories).
% Their setup scripts should find and execute this script.
%
% Example of how to invoke this script:
% 	run(fullfile('<path-to-this-support-module>', 'setup_this_module.m'));
% which might translate into:
%	run ../wave2wire-simulink-support/setup_this_module
%
% The setup involves:
% - Add paths (see below)
% - Run setup of scripts of sub-modules in this module
function setup_this_module()
  this_dir = fileparts(mfilename('fullpath'));
  repository_root_dir = this_dir;
  lib_dir = fullfile(repository_root_dir, 'lib');

  verbose = true;
  show_message(repository_root_dir, verbose);
  setup_support_libraries(lib_dir, false && verbose);
  set_Simulink_encoding_if_needed();
end


function setup_support_libraries(lib_dir, verbose)
  addpath(fullfile(lib_dir, 'misc-octave-and-matlab-tools'));

  toolbox_dir = setup_submodule_by_chr(lib_dir, '1.8.1');
  add_support_for_some_of_CHRs_old_CAS_tools(toolbox_dir, verbose); % For chrMake()
  run(fullfile(toolbox_dir, 'add_paths_for_xls_struct_array.m'));
end


function show_message(repository_root_dir, verbose)
  if ~verbose, return; end
  fprintf('# Setting up module ''%s'' at ''%s''\n', ...
            'wave2wire-simulink-support', repository_root_dir);
end
