%% Add support for some of CHR's old CAS tools.
%
% Note: CAS = Centre for Autonomous Systems. Was located at KTH until 2019.
%
% Caveat: If 'toolbox_dir' is not provided, you must already have run
% the script <chr-toolbox>/add_paths_for_misc.m
function add_support_for_some_of_CHRs_old_CAS_tools(toolbox_dir, verbose)
  if nargin == 0
    toolbox_dir = fileparts(fileparts(which('package_info')));
  end

  %% Add support also for some of CHR's old CAS tools
  run(fullfile(toolbox_dir, 'add_paths_for_CAS.m'));

  if isempty(getenv('CAS'))
    setenv('CAS', fullfile(toolbox_dir, 'CAS'));
    if verbose
      fprintf('#   Environment variable ''%s'' is (now) ''%s''\n', 'CAS', ...
              getenv('CAS'));
    end
  end
end
