function toolbox_dir = setup_submodule_by_chr(lib_dir, required_version)
  toolbox_dir = fullfile(lib_dir, 'chr-octave-and-matlab-tools');
  file = fullfile(toolbox_dir, 'add_paths_for_misc.m');
  assert(exist(file, 'file') == 2, ...
         [ 'Unable to find ''%s''. \n', ...
	   'Did you clone this repository (%s) with ''--recurse-submodules''?' ], ...
         'wave2wire-simulink-support', file);
  run(file);

  %% Check version
  try
    assert_sufficient_package_version(which('package_info'), required_version);
  catch ME
    fprintf('# Error: Unable to find a sufficient version of a submodule\n\tTry:\n');
    fprintf('\t\tgit pull --recurse-submodules\n');
    rethrow(ME);
  end
end


