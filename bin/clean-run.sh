#!/bin/bash -e
#
# Perform a clean simulation run.
#
# Note: This script must be launched from the directory above the
# 'models' directory, as it'll call the script 'clean-update.sh'
# which expects to be in the parent directory.
#
# This might change in the future.
#
# See MATLAB script TBW for what is actually done.
#
# Example:
#	simulink_support/bin/clean-run-01.sh run_script_01
#
# where 'run_script_01' refers to a MATLAB script in
#
#	simulink_support/simulation-run-scripts/
#
# /CHR

printf "Current working directory: '%s'\n" "$(pwd)"

function error() {
    printf "# Error: $1\n" $2 $3 $4 $5 $6
    exit 1
}

if [ "$1" = "" ]; then
    printf "Please provide the name of a run script, e.g. '%s'\n" \
	   sim_run_01
    printf "For scripts, look in the directory: '%s'\n" \
	   simulink_support/simulation-run-scripts

    error "No name of the run script provided"
else
    run_script="$1"
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
config_dir="${script_dir}"
source "${config_dir}/default-path-and-repos.cfg"

initial_dir=$(pwd)

function assert_dir_exists() {
    local work_dir="$1"
    if [ ! -d "$work_dir" ]; then
        error "Directory '%s' not found, clone first\n" "$work_dir"
    fi
}

assert_dir_exists "${support_dir}"
assert_dir_exists "${inputs_dir}"
assert_dir_exists "${models_dir}"
assert_dir_exists "${postpr_dir}"

"${script_dir}/clean-update.sh"


forced_pars_file="${support_dir}/simulation-run-scripts/${run_script}.mat"
printf "Copying forced parameters: '%s' -> '%s'\n" \
	"${forced_pars_file}" "${inputs_dir}/forced_parameters.mat"
cp      "${forced_pars_file}" "${inputs_dir}/forced_parameters.mat"


# MATLAB will be started from within the 'models' directory
# The MATLAB scripts/code to run must therefore also make this assumption.
run_scripts_dir="../${support_dir}/simulation-run-scripts"
run_file="${run_scripts_dir}/${run_script}.m"


# todo: Change location of log file, currently ends up in 'models/'
# so the next time you run this script, that logfile is overwritten
log_file=run-log.txt


pushd "${models_dir}" > /dev/null
printf "Current working directory: '%s'\n" "$(pwd)"
matlab_opts=( -nosplash -wait -nodesktop -logfile "${log_file}" )
echo "# Executing:"
echo "$matlab_executable" "${matlab_opts[@]}" \
    -r "pwd, run ${run_file}"
"$matlab_executable" "${matlab_opts[@]}" \
    -r "pwd, run ${run_file}"
popd > /dev/null
