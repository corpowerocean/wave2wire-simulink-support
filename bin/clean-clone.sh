#!/bin/bash -e
#
# Clone needed repositories.
# Only take a single branch from each repository
# to save disk space and bandwidth.
#
# /CHR

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
config_dir="${script_dir}"
source "${config_dir}/default-path-and-repos.cfg"

function git_clone() {
    local work_dir="$1"
    local repo="$2"
    local branch="$3"
    local git_opts="--depth 1 --recurse-submodules --shallow-submodules"
    printf "\nBranch used in command below: %s\n" "${branch}"
    git clone $git_opts --branch "${branch}" --single-branch "${repo}" "${work_dir}"
}

function error() {
    printf "# Error: $1\n" $2 $3 $4 $5 $6
    exit 1
}

function assert_no_preexisting_dir() {
    local work_dir="$1"
    if [ -d "$work_dir" ]; then
        error "Directory '%s' exists, remove it first\n" "$work_dir"
    fi
}

assert_no_preexisting_dir "${support_dir}"
assert_no_preexisting_dir "${inputs_dir}"
assert_no_preexisting_dir "${models_dir}"
assert_no_preexisting_dir "${postpr_dir}"

git_clone "${support_dir}" "${support_repo}" "${support_branch}"
git_clone "${inputs_dir}"  "${inputs_repo}"  "${inputs_branch}"
git_clone "${models_dir}"  "${models_repo}"  "${models_branch}"
git_clone "${postpr_dir}"  "${postpr_repo}"  "${postpr_branch}"
