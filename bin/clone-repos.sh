#!/bin/bash -e
#
# Support script to make it easier to clone out repositories.
#
# Clones the following repositories.
#	simulink_model/
#	simulink_inputs_SRS_status/
#	simulink_postprocessing/
#
# Instruction:
# - Change directory to two levels above the directory with this script
# - Ensure there isn't already any directories named as per above
# - Execute this script, i.e. typically via
#
#         simulink_support/bin/clone-repos.sh
#
# /CHR

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
config_dir="${script_dir}"
source "${config_dir}/default-path-and-repos.cfg"

function git_clone() {
    local work_dir="$1"
    local repo="$2"
    local branch="$3"
    local git_opts="--recurse-submodules --shallow-submodules"
    printf "\nBranch used in command below: %s\n" "${branch}"
    git clone $git_opts --branch "${branch}" "${repo}" "${work_dir}"
}

function error() {
    printf "# Error: $1\n" $2 $3 $4 $5 $6
    exit 1
}

function assert_no_preexisting_dir() {
    local work_dir="$1"
    if [ -d "$work_dir" ]; then
        error "Directory '%s' exists, remove it first\n" "$work_dir"
    fi
}

assert_no_preexisting_dir "${inputs_dir}"
assert_no_preexisting_dir "${models_dir}"
assert_no_preexisting_dir "${postpr_dir}"

git_clone "${inputs_dir}"  "${inputs_repo}"  "${inputs_branch}"
git_clone "${models_dir}"  "${models_repo}"  "${models_branch}"
git_clone "${postpr_dir}"  "${postpr_repo}"  "${postpr_branch}"
