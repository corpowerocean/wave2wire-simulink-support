#!/bin/bash -e
#
# Clean/update working directories for the needed repositories.
#
# /CHR

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
config_dir="${script_dir}"
source "${config_dir}/default-path-and-repos.cfg"

initial_dir=$(pwd)

function git_clean() {
    local work_dir="$1"
    pushd "${work_dir}" > /dev/null
    git clean -f -x -d
    popd > /dev/null
}

function git_pull() {
    local work_dir="$1"
    pushd "${work_dir}" > /dev/null
    git pull
    popd > /dev/null
}

function git_clean_and_pull() {
    local work_dir="$1"
    printf "# Cleaning and pulling work. dir: '%s'\n" "${work_dir}"
    git_clean "${work_dir}"
    git_pull "${work_dir}"
}

function error() {
    printf "# Error: $1\n" $2 $3 $4 $5 $6
    exit 1
}

function assert_dir_exists() {
    local work_dir="$1"
    if [ ! -d "$work_dir" ]; then
        error "Directory '%s' not found, clone first\n" "$work_dir"
    fi
}

assert_dir_exists "${support_dir}"
assert_dir_exists "${inputs_dir}"
assert_dir_exists "${models_dir}"
assert_dir_exists "${postpr_dir}"

git_clean_and_pull "${support_dir}"
git_clean_and_pull "${inputs_dir}"
git_clean_and_pull "${models_dir}"
git_clean_and_pull "${postpr_dir}"


