function is_avail = is_parallel_toolbox_available()
  versions = ver();
  names = { versions.Name };
  toolbox = 'Parallel Computing Toolbox';
  is_avail = any(cellfun(@(name) isequal(name, toolbox), names));
end
