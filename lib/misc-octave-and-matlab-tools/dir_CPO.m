%% Support function to return a folder location.
%
%!demo % Simple example
%!	d = dir_CPO('<cpo-tech>', 'd1', 'd2')
%
%!demo % Show compact list of available labels
%!      dir_CPO
%
%!demo % Show available labels with more details
%!	dir_CPO --labels;
%
%!demo % Return "data base", e.g. for debugging
%!	dir_data = dir_CPO('--data');
%
%!demo % List files in a directory (activates when there's no output)
%!      dir_CPO <cpo-tech>
%
% See Octave test cases at the end for usage examples
function out = dir_CPO(label, varargin)
  if (nargin == 0 && nargout == 0) || (nargin == 1 && isequal(label, '-l'))
    handle_special_invocations('-l');
    return
  end
  assert(exist('label', 'var') == 1, ...
	 'No argument given for ''%s''', 'label');
  assert(~isempty(label) && ischar(label), ...
	 'Expected non-empty string as first argument');

  d = handle_special_invocations(label, varargin{:});
  if ~isempty(d), out = d; return; end;

  [label, components] = extract_label(label);

  if ~isempty(label)
    components = cat(2, find_dir_components(make_dir_data(), label), components);
  end
  d = fullfile(components{:}, varargin{:});
  if nargout == 0
    fprintf('Listing files in ''%s''\n', d);
    dir(d);
  else
    out = d;
  end
end


function data = make_dir_data()
  persistent cache;
  if isempty(cache)
    win = { 'X:' };		       % Drive letter of network share
    unix = { '/' 'mnt' 'cposhare01' }; % Path to mounted network share
    wNAS = { 'Y:' };		       % Drive letter of NAS server
    uNAS = { '/' 'mnt' 'cpo-nas-01' }; % todo: fix path. Path to mounted network share 

    CPO_Tech = { 'CPO Tech' };
    buoy_design = { CPO_Tech{:} 'Buoy design' };
    W2W_results = { CPO_Tech{:} 'Modeling - Dynamic simulations', ...
		    'Wave2Wire', 'ResultsRepo' };

    %% Note: Labels in the table below must be in upper case, and
    %% using hypens instead of spaces.
    C = { ...
	  'CPO-SHARE'   { win{:} filesep() }         { unix{:} }; ...
	  'CPO-TECH'    { win{:} CPO_Tech{:} }	     { unix{:} CPO_Tech{:} }; ...
	  'CPO-BUOY'    { win{:} buoy_design{:} }    { unix{:} buoy_design{:} }; ...
	  'CPO-W2W-RES' { win{:} W2W_results{:} }    { unix{:} W2W_results{:} }; ...
      'CPO-NAS' { wNAS{:} }    { uNAS{:} }; ...
	  'CPO-W2W-NAS-RES' { wNAS{:} W2W_results{:} }  { uNAS{:} W2W_results{:} } ...
	};
    cache = struct('label', C(:, 1), ...
		   'win',   C(:, 2), ...
		   'unix',  C(:, 3));
  end
  data = cache;
end


function [label, remainder] = extract_label(label)
  if label(1) ~= '<'
    [label, remainder] = deal([], {label});
    return
  end
  assert(label(1) == '<', 'Expected a ''<'' as first label character');
  [label, remain] = strtok(label(2:end), '>');
  assert(length(remain) >= 1 && remain(1) == '>', ...
	 'Expected a ''>'' to end the label');

  if length(remain) > 1
    remainder = {remain(2:end)};
  else
    remainder = {};
  end
end


function entries = find_dir_components(data, label)
  data = make_dir_data();
  lbl = upper(strrep(label, ' ', '-'));
  row = find(strcmp({data.label}, lbl));
  assert(~isempty(row), 'No entry found for label: %s', label);
  assert(numel(row) < 2, 'Multiple entries found');

  if isunix()
    entries = data(row).unix;
  else
    entries = data(row).win;
  end
end


function d = handle_special_invocations(label, varargin)
  switch label
    case '--reverse', d = handle_reverse_replacement(varargin{:});
    case {'--data' '--cache'}, d = make_dir_data();
    case {'--label' '--labels'}, d = show_labels(make_dir_data(), false);
    case {'-l'}, d = show_labels(make_dir_data(), true);
    otherwise d = [];
  end
end


function path = handle_reverse_replacement(path)
  data = make_dir_data();
  for row = size(data,1):-1:1
    if isunix()
      str = fullfile(data(row).unix{:});
    else
      str = fullfile(data(row).win{:});
    end
    if strncmp(str, path, length(str))
      path = strrep(path, str, ['<' data(row).label '>']);
      return;
    end
  end
end


function d = show_labels(data, show_compactly)
  d = { data.label };
  if show_compactly
    labels = cellfun(@(l) ['<' lower(l) '>'], { data.label }, 'uni', 0);
    fprintf('Labels: %s\n', ...
            fold_left(@(a, b) [a ', ' b], labels));
  else
    fprintf('Available "labels" (note: case insensitive): \n');
    arrayfun(@(D) fprintf('\t%-12s\t''%s''\n', ...
			  ['<' lower(D.label) '>'], ...
			  fullfile(D.win{:})), ...
	     data);
  end
end


%%% Test case section
%
% FUT = Function under test. To avoid changing test cases if fcn name changes.
%
%!function fut = FUT()
%!  fut = @dir_CPO;


%!error <No argument given for 'label'> dir_CPO()';
%!error <Expected non-empty string as first argument> dir_CPO(1);


%!test 	% Get root of network share
%!  args = {'<cpo-share>'};
%!  if isunix()
%!    expected.dir = fullfile(filesep(), 'mnt', 'cposhare01');
%!  else
%!    expected.dir = fullfile('X:');
%!  end
%!
%!  fut = FUT();
%!  result.dir = fut(args{:});
%!  assert(result, expected);


%!test  % Get <CPO Tech>
%!  fut = FUT();
%!  args = {'<cpo-tech>'};
%!  expected.dir = fullfile(fut('<cpo-share>'), 'CPO Tech');
%!
%!  result.dir = fut(args{:});
%!  assert(result, expected);


%!test  %% Get <CPO Tech>/dir1/dir2
%!  fut = FUT();
%!  args = {'<cpo-tech>' 'dir1' 'dir2'};
%!  expected.dir = fullfile(fut(args{1}), args{2:end});
%!
%!  result.dir = fut(args{:});
%!  assert(result, expected);
