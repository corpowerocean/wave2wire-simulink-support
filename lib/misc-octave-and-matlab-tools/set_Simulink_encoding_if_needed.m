%% Note: For compatibility across platforms, it's better to use UTF-8
%% as the encoding in Simulink.
function set_Simulink_encoding_if_needed(encoding)
  if nargin == 0, encoding = 'UTF-8'; end;

  if ispc() && is_matlab()
    if ~isequal(encoding, slCharacterEncoding())
      fprintf('# On Windows/MATLAB: Adjust Simulink character encoding to UTF-8\n');
      slCharacterEncoding(encoding)
    end
  end
end
