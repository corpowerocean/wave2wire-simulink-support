%% Return version of the simulink_support repository that's used.
% Optionally also return the (absolute) path to the support root dir.
%
% Caveat: This file _must_ be located two dir levels below the root.
function [version, root] = version_of_simulink_support()
  version = '1.7.1';

  dir = fileparts(mfilename('fullpath'));
  root = fileparts(fileparts(dir));
end
