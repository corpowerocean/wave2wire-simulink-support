%% Support function to create a "simulation suite information" (SSI)
% struct, i.e. details about a number of simulation runs done with the
% Wave2Wire model.
%
% The SSI includes e.g.:
% .name		= "Name" of the simulation suite
% .dir		= Structure with paths to various directories
%   .top	= Top dir of the suite of simulation runs
%   .sim_settings	= Dir with settings files (.mat)
%   .sim_results	= Dir with results files (.mat)
% .nr		= Currently referenced run number in the suite
% .run_numbers	= Run numbers, extracted from file names in results dir
%
% Additionally, the SSI contains some convenience functions:
% .sim_settings_file(SSI, [opt] run_number)
%   - Return path to the .mat-file with sim. settings for run 'run_number'
%
% .sim_results_file(SSI, [opt] run_number)
%   - Return path to the .mat-file with sim. results for run ...
%
% .load_sim_settings(SSI, [opt] run_number)
%   - Return 'output_settings_struct' from sim. settings for run ...
%
% .load_sim_results(SSI, [opt] run_number)
%   - Return 'SimulationResuts' from sim. results fro run ...
%
% .new_number(run_number)
%   - Return a new SSI based on 'SSI.dir.top' and 'run_number'
%
% Note: Assuming something like the following for the directory with the
% simulation suite:
%     <cpo-w2w-res>/MAIN/09-May-2019_HG_<snip>/
%	├── 09-May-2019_HG_<snip>_<run-numbers>.txt
%	├── Log.txt
%	├── PostProcessing
%	│   └── AnalysisDampingVariation.mat
%	├── Results
%	│   ├── 09-May-2019_HG_<snip>_<run-numbers>.mat
%	└── Settings
%	    ├── 09-May-2019_HG_<snip>_<run-numbers>.mat
%	    └── forced_parameters.mat
%
% Example:
%	SSI_dir = dir_CPO('<cpo-w2w-res>', 'MAIN', ...
%                          ['13-May-2019_HG_', ...
%                           'Damping_Te_Variation_WS_PT_connected_WS_A_corr_ForcedInputs']);
%       nr = 154;
%       SSI = make_sim_suite_info(SSI_dir, nr);
%
%       % For convenience, if the first arg is a cellstr, it's passed to
%       % dir_CPO() automatically. So the following can be done:
%       nr = 154;
%       SSI = make_sim_suite_info(...
%                 { '<cpo-w2w-res>', 'MAIN', ...
%                   ['13-May-2019_HG_Damping_Te_Variation_WS_PT_connected_WS_A_corr_ForcedInputs']}, ...
%                  nr);
%        
%
function info = make_sim_suite_info(sim_suite_dir, run_number)
  if nargin == 0, help make_sim_suite_info; end
  if ~exist('sim_suite_dir', 'var'), sim_suite_dir = []; end
  if ~exist('run_number', 'var'), run_number = []; end

  if iscellstr(sim_suite_dir), sim_suite_dir = dir_CPO(sim_suite_dir{:}); end
  if isempty(run_number), run_number = 1; end;

  assert_directory_exists(sim_suite_dir);
  [~, name] = fileparts(sim_suite_dir);

  dirs = struct('top', sim_suite_dir, ...
		'sim_settings', fullfile(sim_suite_dir, 'Settings'), ...
		'sim_results', fullfile(sim_suite_dir, 'Results'));

  info = struct('name', name, ...
		'nr', run_number, ...
		'dir', dirs);

  d = dir(fullfile(info.dir.sim_results, '*.mat'));
  info.run_numbers = extract_run_numbers_from_result_file_names({d.name});

  info.sim_settings_file = @(varargin) sim_settings_file(info, varargin{:});
  info.sim_results_file = @(varargin) sim_results_file(info, varargin{:});

  info.load_sim_settings = @(varargin) load_sim_settings(info, varargin{:});
  info.load_sim_results = @(varargin) load_sim_results(info, varargin{:});

  info.new_number = @(nr) make_sim_suite_info(info.dir.top, nr);
end


function [file, nr] = sim_settings_file(info, run_nr)
  if ~exist('run_nr', 'var'), run_nr = []; end
  if isempty(run_nr), nr = info.nr; else nr = run_nr; end;
  file = fullfile(info.dir.sim_settings, sprintf('%s_%d.mat', info.name, nr));
end


function [file, nr] = sim_results_file(info, run_nr)
  if ~exist('run_nr', 'var'), run_nr = []; end
  if isempty(run_nr), nr = info.nr; else nr = run_nr; end;
  file = fullfile(info.dir.sim_results, sprintf('%s_%d.mat', info.name, nr));
end


function OS = load_sim_settings(sim_suite_info, run_nr)
  if ~exist('run_nr', 'var'), run_nr = []; end
  [file, nr] = sim_settings_file(sim_suite_info, run_nr);
  OS = load_wrapper(file, 'output_settings_struct', nr);
end


function OS = load_sim_results(sim_suite_info, run_nr)
  if ~exist('run_nr', 'var'), run_nr = []; end
  [file, nr] = sim_results_file(sim_suite_info, run_nr);
  OS = load_wrapper(file, 'SimulationResults', nr);
end


function D = load_wrapper(file, field, run_number)
  fprintf('Loading ''%s'' for run %d from \n\t''%s'' ...', ...
	  field, run_number, ...
	  dir_CPO('--reverse', file));
  data = load(file, field);
  fprintf('done\n');
  D = data.(field);
end


function numbers = extract_run_numbers_from_result_file_names(names)
  nr_indices = @(s) (find(s=='_',1,'last')+1) : (find(s=='.',1,'last')-1);
  numbers = sort(cellfun(@(s) str2num(s(nr_indices(s))), names));
end



% <cpo-w2w-res>/MAIN/13-May-2019_HG_Damping_Te_Variation_WS_PT_connected_WS_A_corr_ForcedInputs
function [dir, nr] = default_data_ref_for_debugging()

  switch 0
    case 1, % Found link in script I got from HGA /CHR
      dir = dir_CPO('<cpo-w2w-res>', 'MAIN', ...
		    ['09-May-2019_HG_', ...
		     'Damping_Te_Variation_WS_PT_connected_ForcedInputs']);
      nr = 1;
      
    otherwise
      %% In e-mail on 2019-05-15 from HGA to CHR etc, he wrote:
      % Run ID for 1.75 m, 9s wave (best damping): 154
      dir = dir_CPO('<cpo-w2w-res>', 'MAIN', ...
		    ['13-May-2019_HG_', ...
		     'Damping_Te_Variation_WS_PT_connected_WS_A_corr_ForcedInputs']);
      nr = 154;
  end
end
