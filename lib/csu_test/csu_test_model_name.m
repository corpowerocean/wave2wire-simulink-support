%% Support function to make it convenient for CSU designer
% to not have to specify the name of the model.
%
% Instead this function is assumed to be called by
% csu_test_setup_once() like this:
%     model = csu_test_model_name(dbstack(1));
%
% Caveat: Only tested to work with inputs from call to 'dbstack()'.
%
% See also: csu_test_setup_once()

function name = csu_test_model_name(filename)
  if isstruct(filename), filename = filename(1).file; end
    
  suffix = '_test.m';
  [n_filename, n_suffix] = deal(length(filename), length(suffix));
  assert(n_filename > n_suffix, ...
         'File name is shorter than expected: ''%s''', filename);
  name = filename(1:(n_filename - n_suffix));
end


