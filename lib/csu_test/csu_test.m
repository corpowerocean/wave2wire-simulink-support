%% Help related to CSU tests
%
% This function wraps calls to 'runtests()'. The test cases (TC:s)
% for a CSU, e.g. ME_SSE, is defined in the file 'me_sse_test.m'.
% That .m-file defines a test vector. The CSU is then simulated
% with that test vector as input, and the result is captured.
% A TC typically then compares the result with some expected values.
%
% Notes:
% - Precondition: Paths and bus objects must have been created.
% - Crappy implemenation, places an object in base WS that
%   contains the test vector.
% - Compatibility: Tested in MATLAB R2016b
%
% Example:
%   csu_test me_wae cd_lsy
%
%
% About a CSU test definition:
% - TBW
%
% - setupOnce() - TBW
%
%
% See also: runtests()

function test_result = csu_test(varargin)
  if nargin == 0, help(mfilename()); return; end

  assert(iscellstr(varargin));

  tests = cellfun(@make_test_file_path, varargin, 'uniform', 0);

  fprintf('# Executing: runtests() for\n');
  cellfun(@(file) fprintf('#\t%s\n', file), tests);
  test_result = runtests(tests);
end


function d = csu_dir(csu)
  d = fullfile('ctrl-core', 'csu', csu);
  if ~exist(d, 'dir')
    d = fullfile('data-core', 'csu', csu);
  end
end


function file = make_test_file_path(csu)
  file = fullfile(csu_dir(csu), [csu '_test.m']);
  assert_file_exists(file, 'Unable to find %s', file);
end

