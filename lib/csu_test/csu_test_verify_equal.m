%% Wrapper to use 'verifyEqual()' on multiple output signals from a CSU test.
%
% Example (used from a test case in a <CSU>_test.m-file):
%
%  expect.cd_lsy_x1 = mk(50 * u1);
%  [~, result_values] = csu_test_run_sim(TD);
%  csu_test_verify_equal(TC, result_values, expect, 'AbsTol', 1e-10);
%
% See also: csu_test() and verifyEqual()
function csu_test_verify_equal(TC, result, expect, varargin)
  args = varargin;
  add_signal_name = mod(numel(args), 2) == 0;
  if ~add_signal_name, diagnostic = {}; end

  for F = fieldnames(expect)'
    f = F{1};
    if add_signal_name, diagnostic = { sprintf('Signal %s', f) }; end
    verifyEqual(TC, result.(f).Data, expect.(f).Data, varargin{:}, diagnostic{:});
  end
end
