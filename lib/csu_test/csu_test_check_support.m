%% Support function to compare resulting signal with expected value
% for a CSU test.
%
% Returns true when all values for all signals differ less than
% 'tolerance' (1e-14 by default).
%
% Caveat: Limited functionality, currently mainly demonstration.
%
% See also: csu_test()
function pass = csu_test_check_support(result, expect, tolerance)
  if nargin < 3 || isempty(tolerance), tolerance = 1e-14; end;
  pass = true;
  for F = fieldnames(expect)'
    f = F{1};
    delta = result.(f).Data - expect.(f).Data;
    idx = find(abs(delta) > tolerance);
    if ~isempty(idx)
      fprintf('# %-20s  not as expected as of element %d (%d/%d values differ)\n', ...
              f, idx(1), numel(idx), size(delta,1));
      pass = false;
      return;
    end
  end
end
