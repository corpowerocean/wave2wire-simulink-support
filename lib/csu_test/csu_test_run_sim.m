%% TBW
%
% Note: The output 'out_values' will only contain the outputs if the
% CSU outputs a single bus.
%
function [out, out_values] = csu_test_run_sim(TC)
  assert(~isempty(TC.test_vector_value));

  test_vector_value = struct(TC.model, TC.test_vector_value);

  assignin('base', TC.test_vector_name, test_vector_value);

  out = sim(TC.model, ...
            'SaveOutput',        'On', ...
            'LoadExternalInput', 'On', ...
            'ExternalInput',     TC.external_input_string, ...
            'StopTime',          num2str(TC.stop_time));

  if nargout > 1
    out_bus = get(get(out, 'yout'), upper(TC.model));
    out_values = out_bus.Values;
  end
end
