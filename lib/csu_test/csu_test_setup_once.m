%% TBW
%
% See also: csu_test()
function S = csu_test_setup_once(model, bus_names, dt)
  if isempty(model), model = csu_test_model_name(dbstack(1)); end;

  ensure_system_is_loaded(model);

  if isempty(bus_names), bus_names = determine_input_buses(model); end
  if isempty(dt), dt = 0.001; end; %% todo: Will likely need fix later

  S = struct('model',             model, ...
             'handle',            get_param(model, 'handle'), ...
             'dt',                dt, ...
             'stop_time',         [], ... % Must be set by TC:s
             'test_vector_name',  ['csu_test_vector'], ...
             'test_vector_value', [] ...
            );

  S.external_input_string = make_external_input_string(S, bus_names);

  %% Maybe we'll have to change settings needed to deal with bug
  do_modify_parameters = false;
  if do_modify_parameters
    %% todo: find and resurrect my old function for use on lines below
    % S.original_parameters = ...
    % set_parameters_that_differ(S.handle, ...
    %                           { 'SaveOutput',        'On'; ...
    %                             'LoadExternalInput', 'On' }');
  end
end


function s = make_external_input_string(S, input_bus_names)
  if has_enable_port(S.handle)
    input_bus_names{end+1} = 'enable';
  end
    
  s1 = cellfun(@(bus) sprintf('%s.%s.%s', S.test_vector_name, S.model, bus), ...
               input_bus_names, 'uniform', 0);
  s = strjoin(s1, ', ');
end


function ensure_system_is_loaded(model)
  if ~bdIsLoaded(model)
    load_system(model);
  end
end


function y = has_enable_port(model)
  y = ~isempty(find_system(model, 'SearchDepth', 1, 'BlockType', 'EnablePort'));
end


%% Use convention that the names of the (CSU) model's inports shall
% match the name of the corresponding input bus.
function bus_names = determine_input_buses(model)
  blks = cellstr(find_system(model, 'SearchDepth', 1, 'BlockType', 'Inport'));
  h = cell2mat(get_param(blks, 'handle'));
  bus_names = cellstr(get(h, 'name'));
end
