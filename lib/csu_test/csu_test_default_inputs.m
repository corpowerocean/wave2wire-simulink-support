%% Default input for a CSU test 
%
% See csu_test() for more info. about writing test cases.
%
% This function needs to be called in a "csu_name_test.m" function as it
% recognizes the csu in test from the name and check the .mdl file from the
% CSU.


function [in] = csu_test_default_inputs(t)
    % getting csu name. it works only if the name of the function is "csu_name_test"
    % for example: mh_ccor_test for testing mh_ccor
    st = dbstack;
    temp = strsplit(st(2).file,'_test');    
    csu_name = temp{1};
    % creating input structure
    in = struct();
    % looks for input ports of the csu
    h_Inports=find_system(csu_name,'FindAll','On','SearchDepth',1,'BlockType','Inport');
    % get their names
    Name_Inports=get(h_Inports,'Name');
    % get their bus.
    Bus_Inports =get(h_Inports,'OutDataTypeStr');
    % loop on the inputs
    for input_port = 1:numel(h_Inports)
        % get the bus name
        if iscell(Bus_Inports)
            Bus_temp_name = strsplit(Bus_Inports{input_port});
            in.(Name_Inports{input_port})=default_bus(Bus_temp_name{2},t);
        else
            Bus_temp_name = strsplit(Bus_Inports);
            in.(Name_Inports)=default_bus(Bus_temp_name{2},t);
        end
        % define default input associated to the bus.
        
    end
    h_Enable=find_system('mc_eos','FindAll','On','SearchDepth',1,'Name','Enable');
    if numel(h_Enable)>0
        mk = @(y) timeseries(y, t);
       in.enable = mk(ones(size(t)));
    end
end

function [bus] = default_bus(busname,t)
    % default structure
    bus = struct();
    % create a structure from the bus
    output = Simulink.Bus.createMATLABStruct(busname);
    % makes such that each field become a timeserie, in the same data type.
    % it's a bit ugly but works for now.
    fields = fieldnames(output);
    for field= 1:numel(fields)
        % if data type is struct, need to extract the subfields
        if isa(output.(fields{field}),'struct')
            subfields = fieldnames(output.(fields{field}));
            temp_struct_1 = struct();
            for subfield = 1: numel(subfields)
                temp_struct_2 = output.(fields{field});
                temp_struct_1.(subfields{subfield}) = create_timeserie_from_all_data_type(temp_struct_2.(subfields{subfield}),t);
            end
            bus.(fields{field}) = temp_struct_1;
        else
            % else, deal with any kind of data type.
            bus.(fields{field}) = create_timeserie_from_all_data_type(output.(fields{field}),t);
        end        
    end
end

function out = create_timeserie_from_all_data_type(in,t)
    % timeseries constructor.
    mk = @(y) timeseries(y, t);
    if isa(in,'uint8')
        out = mk(uint8(ones(numel(in),numel(t)))');
    % if data type is uint16
     elseif isa(in,'uint16')
        out = mk(uint16(ones(numel(in),numel(t)))');
    % if data type is logical
    elseif isa(in,'logical')
        out = mk(true(numel(in),numel(t))');
    % if data type is double
    elseif isa(in,'double')
        out = mk((ones(numel(in),numel(t)))');
    else
        warning(['data type not found',class(in)])
        out = mk(ones(numel(in),numel(t))');
    end 
end