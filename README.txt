Wave2Wire Simulink support repository

*Abstract*

A repository with some support functionality for use with the
Wave2Wire (W2W) repositories as well as other repositories.

The scripts kept in this repository were initially mainly intended to
support regression tests. Now the repository also pulls in some
external libraries as 'git submodules'.

Background: Initially needed as the Wave2Wire sources are split up
into multiple repositories.

Note: CHR hopes that sometime in the future this will change, and a
single repository will be used for the Wave2Wire repositories.



* Repository location

The intended location for this repository is:
https://gitlab.com/corpowerocean/wave2wire-simulink-support although
it's "master" location is currently on the file server.  The
repository at GitLab therefore occasionally need to be synchronised.

* Repository tasks
** Cloning this repository

Note: This repository must be cloned with the option to:
: --recurse-submodules

The reason is to include external dependencies (git submodules).

Using Git Bash under Windows 10, you could e.g. clone this repo with the
following recommended commands:

	mkdir -p /c/repos
	cd /c/repos
	export repo_url="https://gitlab.com/corpowerocean/wave2wire-simulink-support.git"
	git clone --recurse-submodules --depth 1 "$repo_url" simulink_support

On *nix, the commands are very similar, just replace the first two commands with

	mkdir -p ~/repos
	cd ~/repos


** Updating this repository

Using e.g. Git Bash under Windows 10:

	cd /c/repos/simulink_support
	git pull --recurse-submodules 


In case of problems, you can try this:

	git submodule update --init --recursive --depth 1


* Regarding regression tests - TBW!

*** Running a regression test (on workstation)

TBW


*** Assumed directory structure (on workstation)

Assumed directory structure (on workstation):

/c/repos/
    regressions-tests/
        simulink_support/	 # This repository
        clean-run-01/
            simulink_inputs/
            simulink_model/
            simulink_postprocessing/
            simulink_support/    # This repository (alt. location)



*** Miscellanous thoughts

Thoughts:

- The Octave/MATLAB script 'simulink_support/setup_this_module.m' should be run to
  e.g. setup paths for this module.

- Therefore the setup scripts of 'simulink_model' and 'simulink_postprocessing',
  i.e. {simulink_model,simulink_postprocessing}/setup_this_module.m,
  need to find and run 'simulink_support/setup_this_module.m'.
  
- Those two setup scripts should, relative to their location, look in
  the following directories (and in this order):
      ../simulink_support/
      ../../simulink_support/


